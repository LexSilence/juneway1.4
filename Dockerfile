FROM debian:10 as build

RUN apt update && apt install -y wget gcc make libpcre3-dev zlib1g-dev libssl-dev
RUN wget http://nginx.org/download/nginx-1.20.2.tar.gz && tar xvfz nginx-1.20.2.tar.gz  && cd /nginx-1.20.2 && ./configure && make && make install

FROM debian:10 
WORKDIR /usr/lib
COPY --from=build usr/lib/mime .
COPY --from=build usr/lib/ssl .
WORKDIR /usr/local/nginx/conf
COPY --from=build /usr/local/nginx/conf/mime.types .
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin/nginx .
RUN mkdir ../logs && touch ../logs/error.log && chmod +x nginx
CMD ["./nginx", "-g", "daemon off;"]
